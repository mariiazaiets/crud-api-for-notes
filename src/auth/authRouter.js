const Router = require('express');
const router = new Router();
const controller = require('./authController');
const {check} = require('express-validator');

router.post('/register', [
  check('username', 'Username cannot be empty').notEmpty(),
  check('password', 'Password cannot be empty').notEmpty(),
], controller.registration);
router.post('/login', controller.login);

module.exports = router;
