const User = require('../models/User');
// const Notes = require('../../models/Notes');
const bcrypt = require('bcrypt');
const {validationResult} = require('express-validator');
const jwt = require('jsonwebtoken');
const {secret} = require('../../config');

const generateAccessToken = (id) => {
  const payload = {id};
  return jwt.sign(payload, secret, {expiresIn: '24h'});
};

class AuthController {
  async registration(req, res) {
    const registrationDate = new Date();
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(400).send({
          message: 'Registration error', errors,
        });
      }
      const {username, password} = req.body;
      const person = await User.findOne({username});

      if (person) {
        res.status(400).send({
          message: 'A user with the same name already exists',
        });
      }

      const hashPassword = bcrypt.hashSync(password, 10);
      // const userNotes = await Notes.find();
      const user = new User({
        username, password:
        hashPassword,
        registrationDate},
      );
      await user.save();
      res.status(200).send({
        message: 'User registered successfully',
      });
    } catch (err) {
      console.log('Error:', err);
      res.status(400).send({
        message: 'Registration error',
      });
    }
  }

  async login(req, res) {
    try {
      const {username, password} = req.body;
      const user = await User.findOne({username});
      if (!user) {
        return res.status(400).send({
          message: 'Invalid username or password',
        });
      }

      // If such a user is found - compare the
      // passwords from the database and the one entered by the user
      const validPassword = bcrypt.compareSync(password, user.password);
      if (!validPassword) {
        return res.status(400).send({
          message: 'Invalid username or password',
        });
      }

      // generate JWT token
      const token = generateAccessToken(user._id);
      return res.status(200).send({
        message: 'Success',
        jwt_token: token,
      });
    } catch (err) {
      console.log('Error:', err);
      res.status(400).send({
        message: 'Login error',
      });
    }
  }
}

module.exports = new AuthController();
