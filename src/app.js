const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const app = express();
const PORT = process.env.PORT || 8080;
const logger = require('morgan');
// const config = require('config.js');
const authRouter = require('./auth/authRouter');
const userRouter = require('./user/userRouter');
const notesRouter = require('./notes/notesRouter');
const {authMiddleware} = require('./middlewares/authMiddleware');


// config
app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
// the program listens to the router
app.use('/api/auth', authRouter);
app.use('/api/users', [authMiddleware], userRouter);
app.use('/api/notes', [authMiddleware], notesRouter);

mongoose.connect('mongodb+srv://Mariia:testuser1@cluster0.hmxlf.mongodb.net/test', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
})
    .then(() => console.log('MongoDB has started ...'))
    .catch((err) => console.log(err));


app.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}`);
});
