const Router = require('express');
const router = new Router();
const controller = require('./notesController');

// router.post('/register', [
//   check('username', 'Username cannot be empty').notEmpty(),
//   check('password', 'Password cannot be empty').notEmpty(),
// ], controller.registration);
// router.post('/login', controller.login);

router.get('/', controller.getNotes)
    .post('/', controller.createNotes)
    .get('/:id', controller.getNotesById)
    .put('/:id', controller.updateNotesById)
    .patch('/:id', controller.checkNotesById)
    .delete('/:id', controller.deleteNoteById);

module.exports = router;
