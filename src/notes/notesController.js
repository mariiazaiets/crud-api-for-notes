const Notes = require('../models/Notes');

class NotesController {
  async createNotes(req, res) {
    try {
      const {text} = req.body;
      await Notes.create({text: text, completed: false, userId: req.user.id});
      res.status(200).send({
        message: 'Success',
      });
    } catch (err) {
      res.status(400).send({
        message: `Error: ${err}`,
      });
    }
  }

  async getNotes(req, res) {
    const offset = req.query.offset ? +req.query.offset : 0;
    const limit = req.query.limit ? +req.query.limit : 0;
    const userId = req.user.id;
    try {
      const notes = await Notes.find({userId})
          .skip(offset)
          .limit(limit);

      const responseArray = notes.map((note) => {
        const doc = note['_doc'];
        return {
          '_id': doc['_id'],
          'userId': doc['userId'],
          'completed': doc['completed'],
          'text': doc['text'],
          'createdDate': doc['createdAt'],
        };
      });

      // get total count
      const count = await Notes.count();
      res.status(200).send({
        offset: offset,
        limit: limit,
        count: count,
        notes: responseArray,
      });
    } catch (err) {
      res.status(400).send({
        message: `Error: ${err}`,
      });
    }
  }

  async getNotesById(req, res) {
    const noteId = req.params.id;
    try {
      const note = await Notes.findById(noteId);

      const doc = note['_doc'];

      const response = {
        '_id': doc['_id'],
        'userId': doc['userId'],
        'completed': doc['completed'],
        'text': doc['text'],
        'createdDate': doc['createdAt'],
      };

      res.status(200).send({note: response});
    } catch (err) {
      res.status(400).send({
        message: `Error: ${err}`,
      });
    }
  }

  async updateNotesById(req, res) {
    const noteId = req.params.id;
    try {
      await Notes.findByIdAndUpdate(noteId, {text: req.body.text});
      res.status(200).send({
        message: 'Success',
      });
    } catch (err) {
      res.status(400).send({
        message: `Error: ${err}`,
      });
    }
  }

  async checkNotesById(req, res) {
    const noteId = req.params.id;
    try {
      const note = await Notes.findById(noteId);
      const completed = note['_doc'].completed;
      await Notes.updateOne({'_id': noteId}, {completed: !completed});
      res.status(200).send({
        message: 'Success',
      });
    } catch (err) {
      res.status(400).send({
        message: `Error: ${err}`,
      });
    }
  }

  async deleteNoteById(req, res) {
    const noteId = req.params.id;
    try {
      await Notes.findOneAndRemove({'_id': noteId});
      res.status(200).send({
        message: 'Success',
      });
    } catch (err) {
      res.status(400).send({
        message: `Error: ${err}`,
      });
    }
  }
}

module.exports = new NotesController();
