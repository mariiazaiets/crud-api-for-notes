const {Schema, model} = require('mongoose');

const Notes = new Schema({
  completed: {type: Boolean, required: true},
  text: {type: String, required: true},
  userId: {type: String, required: true},
}, {timestamps: true});

module.exports = model('Notes', Notes);
