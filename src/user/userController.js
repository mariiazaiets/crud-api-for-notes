const User = require('../models/User');
const bcrypt = require('bcrypt');

class UserController {
  async getProfileInfo(req, res) {
    try {
      const user = await User.findById(req.user.id);
      res.status(200).send({
        user: {
          _id: user['_doc']['_id'],
          username: user['_doc'].username,
          createdDate: user['createdAt'],
        },
      });
    } catch (err) {
      res.status(400).send({
        message: `Error: ${err}`,
      });
    }
  }

  async deleteUserProfile(req, res) {
    try {
      await User.findOneAndRemove({
        '_id': req.user.id,
      });
      res.status(200).send({
        message: 'Success',
      });
    } catch (err) {
      res.status(400).send({
        message: `Error: ${err}`,
      });
    }
  }

  async changeUserPassword(req, res) {
    try {
      const {oldPassword, newPassword} = req.body;
      const user = await User.findById(req.user.id);
      const currPassword = user['_doc']['password'];

      const validPassword = bcrypt.compareSync(oldPassword, currPassword);
      if (!validPassword) {
        return res.status(400).send({
          message: 'The old password is invalid',
        });
      }

      await User.updateOne({'_id': req.user.id},
          {password: bcrypt.hashSync(newPassword, 10)});

      res.status(200).send({
        message: 'Success',
      });
    } catch (err) {
      res.status(400).send({
        message: `Error: ${err}`,
      });
    }
  }
}

module.exports = new UserController();
