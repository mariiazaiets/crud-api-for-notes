const Router = require('express');
const router = new Router();
const controller = require('./userController');

router.get('/me', controller.getProfileInfo)
    .delete('/me', controller.deleteUserProfile)
    .patch('/me', controller.changeUserPassword);

module.exports = router;
